Chapter 16


EXERCISE 16.1


import sys
import os


class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """


def print_time(time_obj):
    hour, minute, second = time_obj.hour, time_obj.minute, time_obj.second
    # Use join to quickly print out string colon separate
    print ':'.join([str(hour), str(minute), str(second)])
    
def main():
    my_time = Time()
    my_time.hour = 11
    my_time.minute = 59
    my_time.second = 30
    print_time(my_time)

if __name__ == '__main__':
    main()

	
	
	
	
	
	
EXERCISE 16.2


import sys
import os


class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """


def compute_scalar_time(time_obj):
    # We create fractional portions of the "hour" attribute
    return time_obj.hour + (time_obj.minute / 60) + (time_obj.second / 3600)


def is_after(time_obj_1, time_obj_2):
    # Get attribute info from objects.
    hour_1, minute_1, second_1 = time_obj_1.hour, time_obj_1.minute, time_obj_1.second
    hour_2, minute_2, second_2 = time_obj_2.hour, time_obj_2.minute, time_obj_2.second
    # Return result from comparing via comput_scalar_time
    return compute_scalar_time(time_obj_1) < compute_scalar_time(time_obj_2)


def main():
    t1 = Time()
    t1.hour = 12
    t1.minute = 59
    t1.second = 30

    t2 = Time()
    t2.hour = 11
    t2.minute = 59
    t2.second = 30    
    # Do comparison of two time objects, True if t1 occurs before t1, False otherwise
    print is_after(t1, t2)

if __name__ == '__main__':
    main()
	
	
	
	
	
	
EXERCISE16.3


import sys
import os


class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """

def print_time(time_obj):
    hour, minute, second = time_obj.hour, time_obj.minute, time_obj.second
    # Use join to quickly print out string colon separated
    if int(second) < 10:
        second = '0' + str(second)

    if int(minute) < 10:
        minute = '0' + str(minute)

    print ':'.join([str(hour), str(minute), str(second)])

def compute_scalar_time(time_obj):
    # We create fractional portions of the "hour" attribute
    return time_obj.hour + (time_obj.minute / 60) + (time_obj.second / 3600)


def is_after(time_obj_1, time_obj_2):
    # Get attribute info from objects.
    hour_1, minute_1, second_1 = time_obj_1.hour, time_obj_1.minute, time_obj_1.second
    hour_2, minute_2, second_2 = time_obj_2.hour, time_obj_2.minute, time_obj_2.second
    # Return result from comparing via comput_scalar_time
    return compute_scalar_time(time_obj_1) < compute_scalar_time(time_obj_2)


def increment(time, seconds): 
    time.second += seconds
    if time.second >= 60: 
        added_min, remaining_sec = divmod(time.second, 60)
        time.second = remaining_sec
        time.minute += added_min
    if time.minute >= 60:
        added_hour, remaining_min = divmod(time.minute, 60)
        time.minute = remaining_min
        time.hour += added_hour
    

def main():
    t1 = Time()
    t1.hour = 12
    t1.minute = 59
    t1.second = 30

    t2 = Time()
    t2.hour = 11
    t2.minute = 59
    t2.second = 30    
    # Do comparison of two time objects, True if t1 occurs before t1, False otherwise
    #print is_after(t1, t2)
    
    print "t1:Before"
    print_time(t1)
    increment(t1, 600)
    print "t1:After"
    print_time(t1)


if __name__ == '__main__':
    main()
	
	
	
	
	
	
EXERCISE 16.4

import sys
import os
from copy import copy

class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """

def print_time(time_obj):
    hour, minute, second = time_obj.hour, time_obj.minute, time_obj.second
    # Use join to quickly print out string colon separated
    if int(second) < 10:
        second = '0' + str(second)

    if int(minute) < 10:
        minute = '0' + str(minute)

    print ':'.join([str(hour), str(minute), str(second)])


def compute_scalar_time(time_obj):
    # We create fractional portions of the "hour" attribute
    return time_obj.hour + (time_obj.minute / 60) + (time_obj.second / 3600)


def is_after(time_obj_1, time_obj_2):
    # Get attribute info from objects.
    hour_1, minute_1, second_1 = time_obj_1.hour, time_obj_1.minute, time_obj_1.second
    hour_2, minute_2, second_2 = time_obj_2.hour, time_obj_2.minute, time_obj_2.second
    # Return result from comparing via comput_scalar_time
    return compute_scalar_time(time_obj_1) < compute_scalar_time(time_obj_2)


def increment(time_obj, seconds):
    ret_time_obj = Time()
    # Copy data from the time_obj to our ret_time_obj
    ret_time_obj = copy(time_obj)
    
    ret_time_obj.second += seconds
    if ret_time_obj.second >= 60: 
        added_min, remaining_sec = divmod(ret_time_obj.second, 60)
        ret_time_obj.second = remaining_sec
        ret_time_obj.minute += added_min
    if ret_time_obj.minute >= 60:
        added_hour, remaining_min = divmod(ret_time_obj.minute, 60)
        ret_time_obj.minute = remaining_min
        ret_time_obj.hour += added_hour
    return ret_time_obj

def main():
    t1 = Time()
    t1.hour = 12
    t1.minute = 59
    t1.second = 30

    t2 = Time()
    t2.hour = 11
    t2.minute = 59
    t2.second = 30    
    # Do comparison of two time objects, True if t1 occurs before t1, False otherwise
    #print is_after(t1, t2)
    
    print "t1:Before increment"
    print_time(t1)
    print 
    print "Printing pure function increment value"
    print_time(increment(t1, 600))
    print
    print "t1:After increment"
    print_time(t1)


if __name__ == '__main__':
    main()
	
	
	
	
	
	
EXERCISE 16.5


import sys
import os
from copy import copy

class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """

def print_time(time_obj):
    hour, minute, second = time_obj.hour, time_obj.minute, time_obj.second
    # Use join to quickly print out string colon separated
    print ':'.join([str(hour).zfill(2), str(minute).zfill(2), str(second).zfill(2)])


def time_to_int(time_obj):
    # We create fractional portions of the "hour" attribute
    minutes = time_obj.hour * 60 + time_obj.minute 
    seconds = minutes * 60 + time_obj.second 
    return seconds


def int_to_time(seconds): 
    time = Time()
    minutes, time.second = divmod(seconds, 60) 
    time.hour, time.minute = divmod(minutes, 60) 
    return time


def is_after(time_obj_1, time_obj_2):
    # Return result from comparing via comput_scalar_time
    return time_to_int(time_obj_1) < time_to_int(time_obj_2)


        
def increment(time_obj, seconds):
    # Turn the time object into seconds
    time_obj_seconds = time_to_int(time_obj)
    ret_time_seconds = time_obj_seconds + seconds
    return int_to_time(ret_time_seconds)


def add_time(t1, t2):
    seconds = time_to_int(t1) + time_to_int(t2) 
    return int_to_time(seconds)


def main():
    t1 = Time()
    t1.hour = 12
    t1.minute = 59
    t1.second = 30

    t2 = Time()
    t2.hour = 11
    t2.minute = 59
    t2.second = 30    
    # Do comparison of two time objects, True if t1 occurs before t1, False otherwise
    print is_after(t1, t2)
    
    print "t1:Before increment"
    print_time(t1)
    print 
    print "Printing pure function increment value"
    print_time(increment(t1, 600))
    print
    print "t1:After increment"
    print_time(t1)


if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 16.6

class Time(object):
    """
    Represents the time of day.
    attributes: hour, minute, second
    """

def time_to_sec(t):
    hours = t.hour * 3600
    minutes = t.minute * 60
    seconds = t.second
    return hours + minutes + seconds

def sec_to_time(s):
    t = Time()
    t.hour = s / 3600
    t.minute = (s % 3600) / 60
    t.second = s % 60
    return t

def multiply_time(t, a):
    seconds = a * time_to_sec(t)
    return sec_to_time(seconds)


# Part 2:

def time_per_mile(t, miles):
    sec = time_to_sec(t) / miles
    return sec_to_time(sec)
	
	
	
	
	
	
EXERCISE 16.7

from datetime import datetime

# Part 1:
def current_day():
    weekdays = [ "Monday", "Tuesday", "Wednesday", "Thursday",
                 "Friday", "Saturday", "Sunday" ]
    today = datetime.today()
    return weekdays[today.weekday()]

#print current_day()


# Part 2:
def birthday(t):
    today = datetime.today()
    age = today.year - t.year
    print "Your age is", age
    
    normalized_birthday = datetime(today.year, t.month, t.day)
    delta = today - normalized_birthday

    print "Your next birthday is in",

    if delta.days > 0:
         print delta
    else:
        today_next_year = today.replace(year = today.year + 1)
        print today_next_year - normalized_birthday
        
#b = datetime(1990, 8, 14)
#birthday(b)


# Part 3:
def double_day(b1, b2):
    delta = b1 - b2
    double_day = b1 + delta
    return double_day

#b1 = datetime(2000, 2, 20)
#b2 = datetime(1990, 8, 14)
#print double_day(b1, b2)

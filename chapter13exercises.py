Chapter 13





EXERCISE 13.1


import argparse
import re
import string

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("source_file", help="The text file containing the source and target urls")
    args = parser.parse_args()
    filename = args.source_file
    f = open(args.source_file,'r')
    
    for line in f:
        for char in string.punctuation:
            line = line.replace(char,'')
        print line.lower().strip()

if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 13.2

import argparse
import re
import string
import collections
import cPickle

# Declare global total word count.
total_word_count = 0

def count_word(word, complete_word_set, book_word_dict, unknown_word_dict):
    global total_word_count
    
    # If we are using a dictionary
    if str(type(complete_word_set)) == "<type 'dict'>":
        try: 
            # See if the word is in our word list dictionary.
            if complete_word_set[word]:
                book_word_dict[word] = book_word_dict.setdefault(word,0) + 1
                total_word_count += 1
        except:
            unknown_word_dict[word] = unknown_word_dict.setdefault(word,0) + 1
            #pass        
    # If we are using a list.
    elif str(type(complete_word_set)) == "<type 'list'>":
        # Use 'in' to see if word is present in valid word list
        if word in complete_word_set:
            book_word_dict[word] = book_word_dict.setdefault(word,0) + 1
            total_word_count += 1

def main():
    # Create argparse object
    parser = argparse.ArgumentParser()
    # Use a positional argument
    parser.add_argument("source_file", help="The text file containing the source and target urls.")
    parser.add_argument("data_struct", help="Select 'list' or 'dict to use in checking for words.")
    parser.add_argument("-c","--count", help="Print out specified number of most frequently used words", type=int)
    # Obtain our positional arguments
    args = parser.parse_args()
    # Set the filename to first positional argument.
    filename = args.source_file
    # Set the data structure type to second positional argument
    data_struct = args.data_struct
    
    # First ensure that we are not picking an unspecified data structure.
    # If we choose a list or dict, we then set 'complete_word_set' accordingly.
    if data_struct not in ['list','dict']:
        print "Unknown data structure '%s': please use 'dict' or 'list'" % data_struct
        exit()
    elif data_struct == 'list':
        word_list_file = open('/Users/tbates/python/Think-Python/think-python/words_list', 'rb')
        complete_word_set = cPickle.load(word_list_file)        
    elif data_struct == 'dict':
        word_list_dict_file = open('/Users/tbates/python/Think-Python/think-python/words_dict', 'rb')
        complete_word_set = cPickle.load(word_list_dict_file)        

    # Open a file handle for the filename passed as argument
    f = open(args.source_file,'r')
    # Create dictionary to store word-specific counts
    book_word_dict = dict()
    unknown_word_dict = dict()
    # We use for loop in order to process each line.
    # Since 'string.punctuation' is literally a string,
    # We use a loop to replace any matching characters with nothing.
    for line in f:
        for char in string.punctuation:
            line = line.replace(char,'')
        # After line is stripped of punctuation, count member words
        # we make all words lowercase, strip whitespace, split on spaces
        # all in one fell swoop.
        for word in line.lower().strip().split():
            count_word(word, complete_word_set, book_word_dict, unknown_word_dict)
            
    # Generate a sorted directory via 'collections' module
    # Sort it based on the count values, and reverse it,
    # so that the largest counts appear first.
    sorted_word_dict =  collections.OrderedDict(sorted(book_word_dict.items(), reverse=True, key=lambda t: t[1]))
    sorted_unknown_word_dict =  collections.OrderedDict(sorted(unknown_word_dict.items(), reverse=True, key=lambda t: t[1]))

    if args.count:
        count = 0
        # Print out sorted directory, keys and values
        for key, value in sorted_word_dict.items():
            if count < args.count:
                #print "%s: %s" % (key,value)
                count += 1

        for key, value in sorted_unknown_word_dict.items():
            if count < args.count:
                print "%s: %s" % (key,value)
                count += 1
        
    # Output our total word count, and unique word count.
    print "Total word count", total_word_count
    print "Number of unique words", len(sorted_word_dict)

if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 13.5

import random
import collections

MAX_TESTS = 10000

def choose_from_hist(hist):
    list_of_vals = list()
    for key in hist:
        # Capture the timse a char occurs
        value = hist[key]
        # As many times as the value indicates, append the key
        # to the list.
        for index in range(value):
           list_of_vals.append(key)
           
    # Now that we have a list of values, we must randomly choose one of them
    # Pick a random integer, between 0 and the length of the result.
    # This is to deal with zero-indexing of lists
    choice_int = random.randint(0, len(list_of_vals) - 1 )
    # Obtain the value in the list at the given random number index
    return random.choice(list_of_vals[choice_int])

        
def histogram(s):
    d = dict()
    for c in s:
        dict_value = d.get(c,0)
        d[c] = dict_value + 1 
    return d

def main():
    # Create a dictionary to store random test results
    result_dict = dict()

    # Create a test historgram
    test_hist = histogram('aab')
    print test_hist
    # Repeat the operation a large number times, see if it aligns with 
    # percentage a char appears. 
    for index in range(MAX_TESTS):
        chosen_value = choose_from_hist(test_hist)
        # We use set default to either add or modify current dictionary value
        result_dict[chosen_value] = result_dict.setdefault(chosen_value,0) + 1
        # We sort the dictionary using the 'collections' module
        sorted_result_dict =  collections.OrderedDict(sorted(result_dict.items(), reverse=True, key=lambda t: t[1]))
    # Print our results, the char, and percentage of times encountered.
    for key, value in sorted_result_dict.items():
           print "%s: %s" % (key, (float(value)/MAX_TESTS) * 100)

if __name__ =='__main__':
    main()
	
	
	
	
	
EXERCISE 13.6

import string
import cPickle

def process_line(line, h):
    # We iterate over the string.punctuation string. This allows us to avoid
    # multiple occurences of an unwanted character.

    for char in string.punctuation:
        line = line.replace(char,' ')

    for word in line.split():
        # Strip the word of punctuation and whitespace all in one swoop
        
        word = word.strip(string.punctuation + string.whitespace)
        # Get the current value and increment. If non-existent, set 
        # the initial value to 0, then increment. This generalizes nicely.
        h[word] = h.get(word, 0) + 1


def process_file(filename):
    # Create a histogram dictionary to count word frequencies
    h = dict()
    # Open the file for reading
    fp = open(filename, 'r')
    # For each line in the file, process the line
    for line in fp:
        # Process the line and provide the histogram dictionary
        process_line(line, h)
    return h


def total_words(h):
    # h.values returns a sequence containing the frequencies of the words
    # stored in the histogram dictionary. We use 'sum' to return the sum
    # of the values in the sequence. So, if a sequence is [1,3 , 4]
    # then 'sum' should return 8.
    return sum(h.values())


def different_words(h):
    # The length of the dictionary is equal to the number of keys
    return len(h)


if __name__ == "__main__":
    # Generate a histogram based on the contents of "Swann's Way"
    hist = process_file('pg7178.txt')
    print "Total amount of words is ", total_words(hist)
    print "Total amount of unique words is ", different_words(hist)

    # Read in our word list via its Cpickle file
    word_list_dict_file = open('/Users/tbates/python/Think-Python/think-python/words_dict', 'rb')
    word_list_dict = cPickle.load(word_list_dict_file)        


    # Now that we have our two dictionaries, we can do set subtraction to see
    # what words do not occur within the dictionary.
    hist_set = set(hist)
    word_list_set = set(word_list_dict)
    
    difference_set =  hist_set - word_list_set 
    print "Total amount of words not found in dictionary: ", different_words(difference_set)
    #print difference_set


	
	
	
	
	
EXERCISE 13.7

import string
import random

def process_line(line, h):
    line = line.replace('-', ' ')
    for word in line.split():
        word = word.strip(string.punctuation + string.whitespace) 
        word = word.lower()
        h[word] = h.get(word, 0) + 1

def process_file(filename): 
    h = dict()
    fp = open(filename) 
    for line in fp:
        process_line(line, h) 
    return h


def total_words(h): 
    return sum(h.values())


def different_words(h): 
    return len(h)


def most_common(h, num=10): 
    t = []
    for key, value in h.items(): 
        t.append((value, key))
        t.sort(reverse=True) 
    return t


def print_most_common(hist, num=10):
    t = most_common(hist)
    print 'The most common words are:' 
    for freq, word in t[0:num]:
        print word, '\t', freq


def random_word(h):
    t=[]
    for word, freq in h.items():
        t.extend([word] * freq)
    return random.choice(t)


def cum_sum(h):
    my_sum = 0
    sum_list = []
    for val in h.values():
        my_sum += val
        # print my_sum
        # the original idea of sum_list[i] = my_sum *does not work*
        sum_list.append(my_sum)
    return sum_list


def bisect(search_term, my_list):

    # Shamelessly stolen from bisect.py
    lo = 0
    hi = len(my_list)
    while lo < hi:
        print "hi: %s lo: %s" % (hi, lo)
        mid = (lo+hi)/2
        if search_term == my_list[mid]:
            print "found search term %s at/near index %s in loop" % (search_term, mid)            
            return mid - 1
        elif search_term < my_list[mid]: 
            hi = mid
        else: 
            lo = mid + 1
    # Since we will likely not precisely find the search term, we may need to 
    # infer a position based on the last values of hi and lo
    print 
    print "out of loop hi: %s lo: %s" % (hi, lo)
    print "found search term %s at/near index %s" % (search_term, mid)

    return mid


if __name__ == "__main__":
    hist = process_file('pg7178.txt')
    # Create a list based on keys of histogram
    word_list = hist.keys()
    # Create a list containing the running cumulative sum of all words
    # use the last list entry, since it is the total
    cumulative_sum = cum_sum(hist)
    #print cumulative_sum
    # Choose random number between 1 to n, cast into string
    random_choice = random.randint(1, cumulative_sum[-1])
    print random_choice
    # Use the bisection search to find where random number is to be found on 
    # the cumulative sum list
    word_index = bisect(random_choice, cumulative_sum)
    print "Random word is ", word_list[word_index]
    print
    print 'Total number of words:', total_words(hist)
    print 'Number of different words:', different_words(hist)
    # print_most_common(hist)
	
	
	
	
	
	
EXERCISE 13.8

import string

import sys 
reload(sys) 
sys.setdefaultencoding("utf-8")

def process_line(line, h, order):
    # Strip out all '-'
    line = line.replace('-', ' ')
    prev_word = str()
    current_word = str()
    word_list = []

    word_tuple = tuple()
    first_word = str()
    second_word = str()
    third_word = str()
    
    for word in line.split():
        word = word.strip(string.punctuation + string.whitespace) 
        word = word.lower()
        # Append the word to the word list
        word_list.append(word)
        print "word list", word_list        
        # If the length of the word list is equal to order plus one
        if len(word_list) == order + 1:
            # Create a tuple. We will need this for an immutabale
            # data structure to use as dictionary key.
            
            # Obtain first and second word from
            first_word = word_list[0]
            second_word = word_list[1]
            third_word = word_list[2]
            # We have a tuple to use for our dict key
            word_tuple = (first_word, second_word)
            print "tuple is ", word_tuple
            # See if the dictionary has a value, based on the key
            # that is a list. If not, create a dummy list
            if type(h.get(word_tuple)) != "<type 'str'>":
                # Set this to an empty list
                h[word_tuple] = []
                # Append the third word to the value pointed to by the tuple
                h[word_tuple].append(third_word) 
            # Pop the earliest entry off the word_list
            word_list.pop(0)                
        #h[word] = h.get(word, 0) + 1
    # Append current word to our word_list
    print "word list", word_list





def process_file(filename, order): 
    h = dict()
    fp = open(filename) 
    for line in fp:
        process_line(line, h, order) 
    return h


def total_words(h): 
    return sum(h.values())


def different_words(h): 
    return len(h)


def most_common(h, num=10): 
    t = []
    for key, value in h.items(): 
        t.append((value, key))
        t.sort(reverse=True) 
    return t


def print_most_common(hist, num=10):
    t = most_common(hist)
    print 'The most common words are:' 
    for freq, word in t[0:num]:
        print word, '\t', freq


def random_word(h):
    t=[]
    for word, freq in h.items():
        t.extend([word] * freq)
    return random.choice(t)


if __name__ == "__main__":
    order = 2
    hist = process_file('pg7178.txt', order)
    print 'Total number of words:', total_words(hist)
    print 'Number of different words:', different_words(hist)
    print_most_common(hist)


EXERCISE 13.9

import string
import math

def zipfs_law():

    def create_dict():
        fin = open('pg1661.txt')
        res = {}
        
        for line in fin:
            line = line.lower()
            for c in string.punctuation:
                line = line.replace(c, " ")
            words = line.split()
            
            for word in words:                
                if word in res:
                    res[word] = res[word] + 1
                else:
                    res[word] = 1
        return res

    def create_ranks(d):
        res = []
        for key, value in d.items():
            res += [(value, key)]
        res.sort(reverse = True)
        return res

    def output(ranks):
        f = open("plot_res.csv", "w")
        for i in range(len(ranks)):
            line = str(math.log(ranks[i][0])) + ", " + str(math.log(i + 1)) + "\n"
            f.write(line)
        f.close()
                   
    d = create_dict()
    ranks = create_ranks(d)
    output(ranks)

zipfs_law()


Chapter 17



EXERCISE 17.1


class Time(object):
    """Represents the time of day.
        attributes: hour, minute, second
    """
    def __init__(self, hour=0, minute=0, second=0):
        self.hour = hour
        self.minute = minute
        self.second = second

    def __str__(self):
        # Use join to quickly print out string colon separated
        return ':'.join([str(self.hour).zfill(2), str(self.minute).zfill(2), str(self.second).zfill(2)])

    def time_to_int(self):
        # We create fractional portions of the "hour" attribute
        minutes = self.hour * 60 + self.minute 
        seconds = minutes * 60 + self.second 
        return seconds

    def increment(self, seconds):
        # Turn the time object into seconds
        """
        :rtype : object
        """
        self_seconds = self.time_to_int()
        ret_time_seconds = self_seconds + seconds
        return self.int_to_time(ret_time_seconds)

    def is_after(self, other):
        # Return result from comparing via compute_scalar_time
        return self.time_to_int() < other.time_to_int()

    def __add__(self, other):
        if isinstance(other, Time):
            return self.increment(other.time_to_int())
        else:
            return self.increment(other)

    @staticmethod
    def int_to_time(seconds):
        time = Time()
        minutes, time.second = divmod(seconds, 60)
        time.hour, time.minute = divmod(minutes, 60)
        return time


def main():
    # Initialize Time objects
    t1 = Time(12, 59, 30)
    t2 = Time(11, 59, 30)

    # Do comparison of two time objects, True if t1 occurs before t1, False otherwise
    print t1.is_after(t2)
    
    print "t1:Before increment"
    print t1
    print 
    print "Printing pure function increment value"
    t1.increment(600)
    print
    print "t1:After increment"
    print t1
    print "Time / distance foolishness"


if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 17.2

class Point(object):
    """Represent point in 2-D space"""
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __str__(self):
        return '{0}, {1}'.format(self.x, self.y)

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        if isinstance(other, tuple):
            # We get first and second items of tuple
            other_x = tuple[0]
            other_y = tuple[1]
            # Add the values to the self Point and return new point
            return Point(self.x + other_x, self.x + other_y)
			
			
			
			
			
EXERCISE 17.4


class Point(object):
    """Represent point in 2-D space"""
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __str__(self):
        return '{0}, {1}'.format(self.x, self.y)

    def __add__(self, other):
        return self.x + other.x, self.y + other.y


p1 = Point(5, 3)
p2 = Point(-3, 4)

print p1 + p2




EXERCISE 17.5 

class Point(object):
    """Represent point in 2-D space"""
    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y

    def __str__(self):
        return '{0}, {1}'.format(self.x, self.y)

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)
        if isinstance(other, tuple):
            # We get first and second items of tuple
            other_x = other[0]
            other_y = other[1]
            # Add the values to the self Point and return new point
            return Point(self.x + other_x, self.x + other_y)


def main():
    p1 = Point(5, 3)
    p2 = Point(-2, 8)
    point_tuple = (3, 5)

    print "Result of p1 + p2", p1 + p2
    print "Result of p1 + point tuple", p1 + point_tuple

if __name__ == "__main__":
    main()
	
	
	


EXERCISE 17.7

class Kangaroo(object):
    """a Kangaroo is a marsupial"""
    
    def __init__(self, contents=[]):
        """initialize the pouch contents; the default value is
        an empty list"""
        self.pouch_contents = contents

    def __str__(self):
        """return a string representaion of this Kangaroo and
        the contents of the pouch, with one item per line"""
        t = [ object.__str__(self) + ' with pouch contents:' ]
        for obj in self.pouch_contents:
            s = '    ' + object.__str__(obj)
            t.append(s)
        return '\n'.join(t)

    def put_in_pouch(self, item):
        """add a new item to the pouch contents"""
        self.pouch_contents.append(item)

kanga = Kangaroo()
roo = Kangaroo()
kanga.put_in_pouch('wallet')
kanga.put_in_pouch('car keys')
kanga.put_in_pouch(roo)

print kanga

# If you run this program as is, it seems to work.
# To see the problem, trying printing roo.



class Kangaroo(object):
    """a Kangaroo is a marsupial"""
    
    def __init__(self, contents=[]):
        # The problem is the default value for contents.
        # Default values get evaluated ONCE, when the function
        # is defined; they don't get evaluated again when the
        # function is called.

        # In this case that means that when __init__ is defined,
        # [] gets evaluated and contents gets a reference to
        # an empty list.

        # After that, every Kangaroo that gets the default
        # value get a reference to THE SAME list.  If any
        # Kangaroo modifies this shared list, they all see
        # the change.

        # The next version of __init__ shows an idiomatic way
        # to avoid this problem.
        self.pouch_contents = contents

    def __init__(self, contents=None):
        # In this version, the default value is None.  When
        # __init__ runs, it checks the value of contents and,
        # if necessary, creates a new empty list.  That way,
        # every Kangaroo that gets the default value get a
        # reference to a different list.

        # As a general rule, you should avoid using a mutable
        # object as a default value, unless you really know
        # what you are doing.
        if contents == None:
            contents = []
        self.pouch_contents = contents

    def __str__(self):
        """return a string representation of this Kangaroo and
        the contents of the pouch, with one item per line"""
        t = [ object.__str__(self) + ' with pouch contents:' ]
        for obj in self.pouch_contents:
            s = '    ' + object.__str__(obj)
            t.append(s)
        return '\n'.join(t)

    def put_in_pouch(self, item):
        """add a new item to the pouch contents"""
        self.pouch_contents.append(item)

kanga = Kangaroo()
roo = Kangaroo()
kanga.put_in_pouch('wallet')
kanga.put_in_pouch('car keys')
kanga.put_in_pouch(roo)

print kanga
print ''

print roo





Chapter 14



EXERCISE 14.1


import sys
import os
import argparse


def walk(dir):
    # Create a list to story names of discovered files
    list_of_names = []
    for name in os.listdir(dir):
        path = os.path.join(dir, name)

        if os.path.isfile(path):
            list_of_names.append(path)
        else:
            walk(path)
    return list_of_names


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", help="The target directory of walk function")
    args = parser.parse_args()
    dir_name = args.dir
    file_list = walk(dir_name)
    print file_list
if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 14.2

import os
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", help="The target directory of walk function")
    args = parser.parse_args()
    dir_name = args.dir
    file_list_tuple = os.walk(dir_name)
    for item in file_list_tuple:
        print item


if __name__ == '__main__':
    main() 
	
	
	
	
	
EXERCISE 14.3



import os
import sys
import cPickle
import anydbm
import anagram_sets


def store_anagrams(d):
    # Create the database
    db = anydbm.open('anagram_shelf','c')
    # Iterate over the dictionary items
    for key, value  in d.items():
        #print "key: %s value:%s" % (key, value)
        # If the anagram is not single character
        if len(value) > 1:
            db[key] = cPickle.dumps(value)
    return db

def read_anagrams(db, search_term):
    # Compute the search_term signature
    search_term_sig = anagram_sets.signature(search_term)
    pickled_search_result = db[search_term_sig]
    return cPickle.loads(pickled_search_result)

def main():
   # First, create your dictionary of anagrams
    d = anagram_sets.all_anagrams('../words.txt')
    # Create our anagram shelf
    anagram_shelf = store_anagrams(d)
    # Test and see if "less" properly returns anagrams
    print read_anagrams(anagram_shelf, 'less')
    # Test and see if "foal" properly returns anagrams
    print read_anagrams(anagram_shelf, 'foal')



if __name__ == '__main__':
    main()
	
	
	
	
	
	
EXERCISE 14.4 

import sys
import os
import collections
import argparse
import re
import collections


MP_PATTERN = ".mp(3|4)$"
MD5_CMD = '/sw/bin/md5sum'
# Create our globally available dictionary
music_dict = {}

def gen_md5sum(full_file_path):
    cmd = MD5_CMD + ' "' + full_file_path + '"'
    #print "cmd is ", cmd
    fp = os.popen(cmd)
    res = fp.read()
    stat = fp.read()
    return res

def check_for_mp_format(item):
    #print "item is ", item
    try:
        # Pattern match the file name for mp3 or mp4 files.
        # 'item' is a large data structure, representing a parent directory
        # containing a bunc of files contained within. 
        parent_dir = item[0]
        candidate_files = item[2]
        for cand_file in candidate_files:
            
            if re.search(".mp(3|4)$", cand_file):
                #print "Found mp(3|4) at ", item
                full_file_path = os.path.join(item[0], cand_file)
                #print "FULL PATH", full_file_path
                # We may have a full path, but may contain spaces
                # Use md5sum function 
                file_md5 = gen_md5sum(full_file_path)
                music_dict[file_md5] = full_file_path
                #print "Result for file %s is %s " % (full_file_path, 
                #    gen_md5sum(full_file_path))
                # Take results from the gen_md5sum function, insert into dictionary.
                # Use md5sum as the key, the filename as value and md5sum. 
                # If we were importing songs, the same md5sum would be equivalent
                # having the same "song" in our db
                
                #print "*" * 20
    except IndexError:
        pass


def recurse_dir(dir_name):
    # Create file list tuple based on specified directory
    file_list_tuple = os.walk(dir_name)
    # Call our check for mp format function on each item
    for item in file_list_tuple:
        check_for_mp_format(item)
        

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", help="The target directory of walk function")
    args = parser.parse_args()
    dir_name = args.dir
    recurse_dir(dir_name)
    print collections.OrderedDict(sorted(music_dict.items(), key=lambda t: t[0]))
if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 14.5

def linecount(filename): 
    count = 0
    for line in open(filename): count += 1
    return count


def return_main_value():
    return __name__

if __name__ == '__main__':
    print linecount('wc.py')
    print "Value of main is ", __name__



EXERCISE 14.6

import urllib
import string

def get_zip():
    zip_code = raw_input("Enter valid US zip Code, e.g. 32707: \n>")
    conn_str = "http://www.uszip.com/zip/" + zip_code
    
    conn = urllib.urlopen(conn_str)
    
    lines = []
    town = ""
    population = 0

    for line in conn:
        lines += [line.strip()]

    for i in range(len(lines)):
        
        if lines[i][:8] == "<hgroup>":
            s = lines[i+2]
            s = s[len("<h2><strong>"):]
            s = s[:s.find("<")]
            s = s.strip()
            
            for c in string.punctuation:
                s = s.replace(c, "")
            
            town = s
                
        if lines[i].lower().find("total population") != -1:
            s = lines[i]
            start = s.find("Total population") + \
                    len("Total population") + len("</dt><dd>")
            end = s.find("<span", start)
            population = s[start:end]
        
    print "You entered %s, which is the zip code for %s." % (zip_code, town)
    print "Population: %s" % (population)

get_zip()



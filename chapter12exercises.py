Chapter 12

EXERCISE 12.1


def sumall(*args):
    t = tuple(args)
    return sum(t)

def main():
    print sumall(1,2,34,5)

if __name__ == '__main__':
    main()
	
	
	
	
	
EXERCISE 12.2

import cPickle
import random


def sort_by_length(words):
    list_of_words = []
    for word in words:
        # (length_of_word, random_number, word)
        list_of_words.append((len(word), random.random(), word))
    
    list_of_words.sort(reverse=True)
    
    res = []
    for length, random_value, word in list_of_words:
        res.append(word)
    return res
    
def main():
    # read in our word list from pickled object
    word_list_file = open('/Users/tbates/python/Think-Python/think-python/words_list', 'rb')
    words = cPickle.load(word_list_file)
    long_to_short_words = sort_by_length(words)
    for word in long_to_short_words:
        print word

if __name__ == '__main__':
	main()
	
	
	
	
	
EXERCISE 12.3

import re
letter_pattern = re.compile('[a-zA-Z]')

def compare(a,b):
    # sort on descending index
    return cmp(b[1], a[1])

def most_frequent(text_sample):
    # Create a list to store our tuple of letters and their counts
    list_of_letter_counts = []
    # Create dictionary to actually store key/value pairs of letters and
    # their respective counts.
    
    letter_count_dict = dict()
    for letter in text_sample:
        # Only match text that is a letter, using letter_pattern variable
        if letter_pattern.match(letter):
            letter = letter.lower()
            # Use setdefault. If no key, add key set to 0, then incremented 1.
            # If key already exists, it will be returned and incremented 1.        
            letter_count_dict[letter] = letter_count_dict.setdefault(letter,0) + 1
    # Use the .items() method to create a list of tuples containing
    # the letter and the count of occurrence.
    list_of_letter_counts = letter_count_dict.items()
    # Reverse sort the list of tuples
    list_of_letter_counts.sort(compare)
    
    res = []
    for letter,count in list_of_letter_counts:
        res.append((letter,count))
    return res
    
def main():
    paragraph = """
    The enigmatic Italy international flew into Milan's Malpensa airport prior to undergoing a medical after the two clubs agreed a fee for the 22-year-old on Tuesday.
    There were chaotic scenes in Milan, with police forced to use smoke bombs to disperse jubilant fans crowding round a restaurant where Balotelli was meeting with club vice-president Adriano Galliani.
    Balotelli, who moved to the Premier League from Inter Milan in August 2010, is expected to put pen-to-paper on a four-and-a-half-year contract before Thursday's transfer deadline.
    The striker has endured a miserable time with City this season, scoring just one Premier League goal, and with his last start coming in the 3-2 derby defeat by Manchester United on 9 December, when he was hauled off after 52 minutes by a clearly frustrated Roberto Mancini.
    Balotelli is confident he will rediscover his best form away from the Etihad Stadium and is hoping that he will receive nothing but the full backing of the Milan supporters.
    Speaking to the Milan Channel, he admitted: "I ran to Milan.
    "I had wanted to play here for a long time. The start of this season was negative, but with this jersey I will do better.
    "I made some important choices, so I hope they can bring good luck to me and the team. I just want the fans to love me."
    Milan vice-president Adriano Galliani has admitted the transfer is a 'dream come true' and believes the arrival of Balotelli can only drive the Italian giants on to greater heights.
    Galliani told the club's official website: "It is a dream come true and something that we all wanted, with the president Silvio Berlusconi at the forefront.
    "With the arrival of Mario, we have strengthened our team a lot.
    "We have worked so hard and Mario has been in our hearts for a long time and finally we have succeeded in signing him."
    To find out more about live football on Sky sports, Click here
"""   
    print most_frequent(paragraph)

if __name__ == '__main__':
	main()
	
	
	


EXERCISE: 12.3

def letter_frequencies(filename):

    freq = {}
    output = []
    fin = open(filename).read()
        
    for char in fin:
        if char.isalpha():
            char = char.lower()        
            if char in freq:
                freq[char] += 1
            else:
                freq[char] = 1

    for key, value in freq.items():
        output.append((value, key))

    output.sort(reverse = True)

    for (count, letter) in output:
        print letter, count


		
		
		
EXERCISE: 12.4

def find_anagrams():

    def create_dict():
        fin = open('words.txt')
        res = {}
        
        for line in fin:
            word = line.strip()
            s = list(word)
            s.sort()

            sorted_char_string = ""
            for char in s:
                sorted_char_string += char

            if sorted_char_string in res:
                res[sorted_char_string] += [word]
            else:
                res[sorted_char_string] = [word]
            
        return res

    def sort_dict(d):
        res = []
        
        for key, value in d.items():
            res += [(len(value), value)]
        return res
                    
    dictionary = create_dict()
    return sort_dict(dictionary)

# 1&2
def print_anagrams(anagram_list, sorted):
    if sorted:
        anagram_list.sort(reverse = True)
    for (count, anagrams) in anagram_list:
        if count >= 2:
            print count, anagrams

# 3      
def scrabble_bingos(anagram_list):
    #anagram_list.sort(reverse = True)

    res = []
    for (count, anagrams) in anagram_list:
        if len(anagrams[0]) == 8 and count >= 2:
            res += [(count, anagrams)]
            
    res.sort(reverse = True)
    print res[0]
            
anagram_list = find_anagrams()
#print_anagrams(anagram_list, True)
scrabble_bingos(anagram_list)



EXERCISE: 12.5

def metathesis_pairs():

    def create_dict():
        fin = open('words.txt')
        res = {}
        
        for line in fin:
            word = line.strip()
            res[word] = []

        return res

    def find_pairs(d):
        res = []
        
        for key, value in d.items():
            k = list(key)
            for i in range(len(key) - 1):
                for j in range(1, len(key)):
                    k[i], k[j] = k[j],  k[i]
                    candidate = make_string(k)
                    if candidate in d:
                        print key, candidate
        return res

    def make_string(k):
        res = ""
        for element in k:
            res += element
        return res
                    
    d = create_dict()
    find_pairs(d)

print metathesis_pairs()


EXERCISE 12.6

def reduce_words():

    def create_dict():
        fin = open('words.txt')
        res = {}
        
        for line in fin:
            word = line.strip()
            res[word] = []
            
        for element in ["a", "i", ""]:
            res[element] = []

        return res

    def add_children(d):
        for key in d.keys():
            children = []
            for i in range(len(key)):
                candidate = key[:i] + key[i+1:]
                if candidate in d and candidate not in children:
                    children.append(candidate)
            d[key] = children
            
        return d

    def recursive_trails(d):
        res = []

        def helper(key, result):
            if d[key] == []:
                return
            if key in ["a", "i", ""]:
                res.append((len(result[0]), result))
            else:
                for entry in d[key]:
                    return helper(entry, result + [entry])
                
        for key,value in d.items():
            helper(key, [key])

        return res

    def top_n(results, n):
        results.sort(reverse = True)
        for i in range(n):
            print results[i]
                    
    d = create_dict()
    add_children(d)    # creates dictionary entries for words that are reducible
    trails = recursive_trails(d)
    top_n(trails, 20)

reduce_words()







